#!/bin/bash
#
#  Copyright (C) 2017 Codethink Limited
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2 of the License, or (at your option) any later version.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library. If not, see <http://www.gnu.org/licenses/>.
#
#  Authors:
#        Tristan Van Berkom <tristan.vanberkom@codethink.co.uk>

topdir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
workdir="${topdir}/work"
vendordir="${workdir}/vendor"
ostree_branch="crates-vendoring"

#####################################################
#          Usage and command line parsing           #
#####################################################
function usage () {
    echo "Usage: "
    echo "  vendor.sh [OPTIONS]"
    echo
    echo "Obtains new crate versions and updates a vendor repository"
    echo
    echo "Options:"
    echo
    echo "  -h --help                    Display this help message and exit"
    echo "  -o --ostree   <directory>    A directory of a local ostree repository to commit to"
    echo "  --date        <date>         Optional date stamp to use"
    echo "  -g --gpg      <gpg>          A GPG key to sign the ostree commits with"
    echo "  --gpg-home    <directory>    A GPG home directory, e.g. /home/user/.gnupg"
    echo "  "
    exit 1;
}

arg_ostree=
arg_date=$(date +%Y-%m-%d-%H%M%S)
arg_gpg=""
arg_gpg_home=""

while : ; do
    case "$1" in 
	-h|--help)
	    usage;
	    shift ;;

	-o|--ostree)
	    arg_ostree=${2};
	    shift 2 ;;

	--date)
	    arg_date=${2};
	    shift 2 ;;

	-g|--gpg)
	    arg_gpg=${2};
	    shift 2 ;;

	--gpg-home)
	    arg_gpg_home=${2};
	    shift 2 ;;

	*)
	    if [ ! -z "${1}" ]; then
		echo "Unrecognized argument ${1}"
		usage
	    fi
	    break ;;
    esac
done

if [ -z "${arg_ostree}" ]; then
    echo "Must specify ostree repository directory"
    echo
    usage
fi

# Prepare some optional GPG args
gpg_args=
if [ ! -z "${arg_gpg}" ]; then
    gpg_args="--gpg-sign=${arg_gpg}"
fi

gpg_home_args=
if [ ! -z "${arg_gpg_home}" ]; then
    gpg_home_args="--gpg-homedir=${arg_gpg_home}"
fi

##########################################
#                Utilities               #
##########################################

# selfUpdate()
#
# Updates this repository, so that Cargo.toml is updated
# and we always automatically pull in new changes
#
function selfUpdate() {
    cd "${topdir}"
    git pull --rebase
}

# commitWorkDir()
#
# Creates a commit to the ostree repo from the work dir
#
#   $1 - Commit message
#
function commitWorkDir() {
    message=${1}

    ostree commit --repo "${arg_ostree}" --branch "${ostree_branch}" --skip-if-unchanged ${gpg_args} ${gpg_home_args} -s "${message}" "${workdir}"
    ostree summary -u --repo "${arg_ostree}" ${gpg_args} ${gpg_home_args}
}

# checkoutWorkDir()
#
# Checks out the work directory from the ostree for updates
#
function checkoutWorkDir() {

    # Remove the work directory
    rm -rf "${workdir}"

    # Checkout
    ostree checkout --repo "${arg_ostree}" "${ostree_branch}" "${workdir}"
}

# ensureOstree()
#
# Ensure that the ostree repo has been created and has an (empty)
# branch to checkout
#
function ensureOstree() {

    if [ ! -d "${arg_ostree}" ]; then

	# Generate the repo
	ostree init --mode=archive-z2 --repo="${arg_ostree}"

	# Create an empty vendor directory
	mkdir -p "${vendordir}"

	# Create an initial commit with empty vendor directory, makes following code easier
	commitWorkDir "Initial Commit"

	# Remove the work directory
	rm -rf "${workdir}"
    fi
}

# pullNewCrates()
#
# Refresh the vendor database in ${vendordir}
#
function pullNewCrates() {

    cd "${topdir}"
    rm -f Cargo.lock
    cargo fetch
    cargo vendor --no-delete -v "${vendordir}"
    rm -f Cargo.lock
}

##########################################
#                   Main                 #
##########################################

selfUpdate
ensureOstree
checkoutWorkDir
pullNewCrates
commitWorkDir "Automatic update of vendor crates database (${arg_date})"

# Cleanup at the end
rm -rf "${workdir}"
