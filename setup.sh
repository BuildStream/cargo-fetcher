#!/bin/bash
#
#  Copyright (C) 2017 Codethink Limited
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2 of the License, or (at your option) any later version.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library. If not, see <http://www.gnu.org/licenses/>.
#
#  Authors:
#        Tristan Van Berkom <tristan.vanberkom@codethink.co.uk>
topdir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

# default options
arg_export=${topdir}/export
arg_schedule=
arg_gpg=
arg_gpg_home=

function usage () {
    echo "Usage: "
    echo "  setup.sh [OPTIONS]"
    echo
    echo "Setup a machine for vendoring cargo to an ostree repo"
    echo
    echo "Options:"
    echo
    echo "  -h --help                     Display this help message and exit"
    echo "  -w --export    <directory>    The directory to export the output logs and ostree repo at"
    echo "  -s --schedule  <expression>   A cron expression indicating when the cargo fetches should run"
    echo "  -g --gpg       <gpg>          A GPG key to sign the ostree commits with"
    echo "  --gpg-home     <directory>    A GPG home directory, e.g. /home/user/.gnupg"
}

while : ; do
    case "$1" in
	-h|--help)
	    usage;
	    exit 0;
	    shift ;;

	-w|--export)
	    arg_export=${2}
	    shift 2 ;;

	-s|--schedule)
	    arg_schedule=${2}
	    shift 2 ;;

	-g|--gpg)
	    arg_gpg=${2};
	    shift 2 ;;

	--gpg-home)
	    arg_gpg_home=${2};
	    shift 2 ;;

	*)
	    break ;;
    esac
done

function dienow() {
    echo ": $1" 1>&2
    exit 1
}

#
# Some sanity checks and path resolutions
#
mkdir -p "${arg_export}" || dienow "Failed to create export directory: ${arg_export}"
arg_export="$(cd ${arg_export} && pwd)"

# Ensure the build schedule for either unconditional
# or continuous builds.
#
#  $1 - "continuous" or "unconditional"
#
function ensureBuildSchedule () {
    local job=
    local scriptname=

    # Create the launch script based on our current configuration
    # and ensure that there is an entry in the user's crontab for
    # the launcher.
    #
    scriptname="launch-cargo-fetcher.sh"
    job="${arg_schedule} ${topdir}/${scriptname}"

    sed -e "s|@@EXPORTDIR@@|${arg_export}|g" \
        -e "s|@@TOPDIR@@|${topdir}|g" \
        -e "s|@@GPGKEY@@|${arg_gpg}|g" \
        -e "s|@@GPGHOME@@|${arg_gpg_home}|g" \
	${topdir}/launcher.sh.in > ${topdir}/${scriptname}
    
    chmod +x ${topdir}/${scriptname}

    # Register the job with user's crontab
    cat <(fgrep -i -v "${scriptname}" <(crontab -l)) <(echo "$job") | crontab -
}

#
# Main
#

# Schedule or change schedule of the nightlies 
if [ ! -z "${arg_schedule}" ]; then
    ensureBuildSchedule
else
    echo "Must specify a cron schedule"
    echo
    usage
    exit 1
fi
